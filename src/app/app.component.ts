import { Component, OnInit } from '@angular/core';
import { ValuesService } from './services/values.service';
import { JsonApiModel, JsonApiQueryData } from 'angular2-jsonapi';
import { Indicator } from './models/indicator';
import { Tmc } from './models/tmc';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  title = 'cumplofront';

  ufs;
  dollars;
  tmcs;
  resume_tmcs;
  resume_dollars;
  resume_ufs;
  date_from;
  date_to;
  showSpinner = false;

  constructor(private valuesServices: ValuesService) { }

  loadData() {
    this.ufs = null;
    this.dollars = null;
    this.tmcs = null;
    this.resume_dollars = null;
    this.resume_tmcs = null;
    this.resume_ufs = null;

    if (this.date_from != null && this.date_from  != null ) {
      this.showSpinner = true;
      this.valuesServices.getUfs(this.getStringDate(this.date_from), this.getStringDate(this.date_to)).subscribe(
        (values: JsonApiQueryData<Indicator>) => {
          this.ufs = values.getModels();
          this.resume_ufs = values.getMeta().meta;
          this.showSpinner = false;
        }
      );
      this.valuesServices.getDollars(this.getStringDate(this.date_from), this.getStringDate(this.date_to)).subscribe(
        (values: JsonApiQueryData<Indicator>) => {
          this.dollars = values.getModels();
          this.resume_dollars = values.getMeta().meta;
          this.showSpinner = false;

        }
      );
      this.valuesServices.getTmcs(this.getStringDate(this.date_from), this.getStringDate(this.date_to)).subscribe(
        (values: JsonApiQueryData<Tmc>) => {
          console.log(values);
          this.tmcs = values.getModels();
          this.resume_tmcs = values.getMeta().meta.maximums;
          console.log(this.resume_tmcs);
          this.showSpinner = false;

        }
      )
    }
  }

  getStringDate(date): string {
    return date.year + '/' + date.month + '/' + date.day;
  }
}
