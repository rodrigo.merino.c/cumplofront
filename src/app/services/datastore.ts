import { JsonApiDatastoreConfig, JsonApiDatastore, DatastoreConfig } from 'angular2-jsonapi';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Indicator } from '../models/indicator';
import { Tmc } from '../models/tmc';
import { environment } from 'src/environments/environment';

const config: DatastoreConfig = {
  baseUrl: environment.url_api,
  models: {
    indicators: Indicator,
    tmcs: Tmc
  }
}

@Injectable()
@JsonApiDatastoreConfig(config)
export class Datastore extends JsonApiDatastore {

    constructor(http: HttpClient) {
        super(http);
    }
  }
