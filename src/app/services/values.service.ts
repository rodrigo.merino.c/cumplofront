import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Datastore } from './datastore';
import { Indicator } from '../models/indicator';
import { Tmc } from '../models/tmc';

@Injectable({
  providedIn: 'root'
})
export class ValuesService {

  constructor(private datastore: Datastore) { }

  getDollars(date_from, date_to): Observable<any> {
    return this.datastore.findAll(Indicator, {date_from: date_from, date_to: date_to, type_value: 'dolar' });
  }
  getUfs(date_from, date_to): Observable<any> {
    return this.datastore.findAll(Indicator, {date_from: date_from, date_to: date_to, type_value: 'uf' });
  }

  getTmcs(date_from, date_to): Observable<any> {
    const result = this.datastore.findAll(Tmc, {date_from: date_from, date_to: date_to });
    return result;
  }
}
