import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';

@JsonApiModelConfig({
    type: 'tmcs'
})

export class Tmc extends JsonApiModel {
  @Attribute()
  value: number;

  @Attribute()
  title: string;

  @Attribute()
  subtitle: string;

  @Attribute()
  date_from: string;

  @Attribute()
  date_to: string;

  @Attribute()
  type_tmc: string;
}
