import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';

@JsonApiModelConfig({
    type: 'indicators'
})
export class Indicator extends JsonApiModel {
  @Attribute()
  value: number;

  @Attribute()
  value_date: string;

}
