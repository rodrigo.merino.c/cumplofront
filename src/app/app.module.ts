import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { ListValuesComponent } from './components/list-values/list-values.component';
import { HttpClientModule } from '@angular/common/http';
import { JsonApiModule } from 'angular2-jsonapi';
import { Datastore } from './services/datastore';
import { ValuesService } from './services/values.service';
import { FiltersComponent } from './components/filters/filters.component';
import { NvD3Module } from 'ng2-nvd3';
import { FormsModule } from '@angular/forms';

// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';
import { LinechartComponent } from './components/linechart/linechart.component';
import { HeaderComponent } from './components/header/header.component';
import { ResumeCardComponent } from './components/resume-card/resume-card.component';
import { BarchartComponent } from './components/barchart/barchart.component';
import { ListtmcComponent } from './components/listtmc/listtmc.component';
import { AtomSpinnerModule } from 'angular-epic-spinners';

@NgModule({
  declarations: [
    AppComponent,
    ListValuesComponent,
    FiltersComponent,
    LinechartComponent,
    HeaderComponent,
    ResumeCardComponent,
    BarchartComponent,
    ListtmcComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AtomSpinnerModule,
    NgbModule,
    JsonApiModule,
    HttpClientModule,
    NvD3Module
  ],
  providers: [Datastore, ValuesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
