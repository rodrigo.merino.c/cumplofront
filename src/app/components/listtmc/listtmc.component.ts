import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-listtmc',
  templateUrl: './listtmc.component.html',
  styleUrls: ['./listtmc.component.scss']
})
export class ListtmcComponent implements OnInit {
  @Input() tmcs;
  constructor() { }

  ngOnInit() {
  }

}
