import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListtmcComponent } from './listtmc.component';

describe('ListtmcComponent', () => {
  let component: ListtmcComponent;
  let fixture: ComponentFixture<ListtmcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListtmcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListtmcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
