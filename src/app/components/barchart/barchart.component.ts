import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
declare let d3: any;

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.scss']
})
export class BarchartComponent implements OnInit, OnChanges {

  @Input() resume_tmcs;


  constructor() { }

  options;
  data;
  ngOnInit() { }
  ngOnChanges(changes: SimpleChanges) {

    this.options = {
      chart: {
        type: 'discreteBarChart',
        height: 450,
        width: 600,
        margin : {
          top: 20,
          right: 20,
          bottom: 50,
          left: 55
        },
        x: function(d){return d.label;},
        y: function(d){return d.value;},
        showValues: true,
        valueFormat: function(d){
          return d3.format(',.4f')(d);
        },
        duration: 500,
        xAxis: {
          axisLabel: 'Tipos'
        },
        yAxis: {
          axisLabel: 'Valores',
          axisLabelDistance: 1
        }
      }
    }
    this.data = [
      {
        key: "Maximos valores por tipo",
        values: this.getValues()
      }
    ];
  }

  getValues() {

    const result = [];

    Object.keys(this.resume_tmcs).forEach(key => {
      const temp = {label: key, value: this.resume_tmcs[key] }
      result.push(temp);
    });
    console.log(result);
    return result;
  }
}
