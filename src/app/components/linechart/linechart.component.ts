import { Component, OnInit ,ViewEncapsulation, Input, SimpleChanges, OnChanges} from '@angular/core';
declare let d3: any;

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.scss'
  ]
})
export class LinechartComponent implements OnInit, OnChanges {



  constructor() { }

  @Input() values;
  @Input() type;
  options;
  data;

  ngOnChanges(changes: SimpleChanges) {

      this.data = [
        {
            key: this.type,
            values: this.getArrayValues(this.values),
            color: '#00FF00'
        }];
      this.options = {
        chart: {
          type: 'lineChart',
          height: 700,
          width: 700,
          margin : {
            top: 20,
            right: 20,
            bottom: 40,
            left: 55
          },
          x: function(d){ return d.x; },
          y: function(d){ return d.y; },
          useInteractiveGuideline: true,
          xAxis: {
            axisLabel: 'Fecha',
            tickFormat: function(d) {
              return d3.time.format('%d/%m/%y')(new Date(d));
           },
          },
          yAxis: {
            axisLabel: 'Valor',
            tickFormat: function(d){
              return d3.format('.02f')(d);
            },
            axisLabelDistance: -10
          }
        }

    };



  }

  getArrayValues(data): any {

    const result = [];
    data.forEach(element => {
      const temp = {x: new Date(element.value_date), y: element.value };
      result.push(temp);
    });
    return result;
  }
  ngOnInit() {

  }
}
