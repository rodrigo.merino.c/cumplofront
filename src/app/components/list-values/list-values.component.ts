import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { ValuesService } from 'src/app/services/values.service';
import { Indicator } from 'src/app/models/indicator';
import { JsonApiQueryData } from 'angular2-jsonapi';

@Component({
  selector: 'app-list-values',
  templateUrl: './list-values.component.html',
  styleUrls: ['./list-values.component.scss']
})
export class ListValuesComponent implements OnInit, OnChanges {
  @Input() values;
  @Input() type;
  constructor(private valuesServices: ValuesService) { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {


  }

}
